<?php

namespace Keszei\Crud\Action;

use ArrayIterator;
use IteratorAggregate;
use Keszei\Crud\Responder\ListResponse as ListResponseInterface;

class ListResponse implements ListResponseInterface, IteratorAggregate {

	private $models = [];

	public function __construct($models) {
		$this->models = $models;
	}

	public function getModels() {
		return $this->models;
	}

	public function getIterator() {
		return new ArrayIterator($this->models);
	}

}
