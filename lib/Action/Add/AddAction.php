<?php

namespace Keszei\Crud\Action\Add;

use Keszei\Action\Exception\UnexpectedType;
use Keszei\Action\AbstractAction;
use Keszei\Action\Model\Request;
use Keszei\Crud\Action\ModelResponse;
use Keszei\Crud\Gateway\PersisterGateway;
use Keszei\Crud\Model\ModelFactory;

class AddAction extends AbstractAction {

	/**
	 * @var ModelFactory
	 */
	private $modelFactory;

	/**
	 * @var PersisterGateway
	 */
	private $gateway;

	public function run(Request $request) {
		$this->persistAndRespond($this->createModel($request));
	}

	private function persistAndRespond($model) {
		$this->gateway->persist($model);
		$this->getResponder()->setResponse(new ModelResponse($model));
	}

	private function createModel(Request $request) {
		$model = $this->modelFactory->create($request);
		if (null === $model) {
			throw new UnexpectedType;
		}

		return $model;
	}

	public function setModelFactory(ModelFactory $modelFactory) {
		$this->modelFactory = $modelFactory;
		return $this;
	}

	public function setGateway(PersisterGateway $gateway) {
		$this->gateway = $gateway;
		return $this;
	}

}
