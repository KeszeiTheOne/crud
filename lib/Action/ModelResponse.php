<?php

namespace Keszei\Crud\Action;

use Keszei\Crud\Responder\ModelResponse as ModelResponseInterface;

class ModelResponse implements ModelResponseInterface {

	private $model;

	public function __construct($model) {
		$this->model = $model;
	}

	public function getModel() {
		return $this->model;
	}

}
