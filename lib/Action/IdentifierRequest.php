<?php

namespace Keszei\Crud\Action;

use Keszei\Action\Model\Request;

interface IdentifierRequest extends Request {

	public function getId();
}
