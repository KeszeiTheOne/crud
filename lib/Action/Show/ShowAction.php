<?php

namespace Keszei\Crud\Action\Show;

use Keszei\Action\AbstractAction;
use Keszei\Action\Exception\UnexpectedType;
use Keszei\Action\Model\Request;
use Keszei\Crud\Action\Exception\ModelNotFound;
use Keszei\Crud\Action\ModelResponse;
use Keszei\Crud\Gateway\FinderGateway;
use Keszei\Crud\Model\ModelFactory;

class ShowAction extends AbstractAction {

	/**
	 * @var ModelFactory
	 */
	private $criteriaFactory;

	/**
	 * @var FinderGateway
	 */
	private $gateway;

	public function run(Request $request) {
		$this->respond($this->findModel($request));
	}

	private function respond($model) {
		$this->getResponder()->setResponse(new ModelResponse($model));
	}

	private function findModel(Request $request) {
		$model = $this->gateway->find($criteria = $this->createCriteria($request));

		if (null === $model) {
			throw new ModelNotFound($criteria);
		}
		
		return $model;
	}

	private function createCriteria(Request $request) {
		$criteria = $this->criteriaFactory->create($request);

		if (null === $criteria) {
			throw new UnexpectedType;
		}

		return $criteria;
	}

	public function setCriteriaFactory(ModelFactory $criteriaFactory) {
		$this->criteriaFactory = $criteriaFactory;
		return $this;
	}

	public function setGateway(FinderGateway $gateway) {
		$this->gateway = $gateway;
		return $this;
	}

}
