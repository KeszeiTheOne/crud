<?php

namespace Keszei\Crud\Action;

class IdRequest implements IdentifierRequest {

	private $id;

	public function __construct($id) {
		$this->id = $id;
	}

	public function getId() {
		return $this->id;
	}

}
