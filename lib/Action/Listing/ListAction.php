<?php

namespace Keszei\Crud\Action\Listing;

use Keszei\Action\AbstractAction;
use Keszei\Action\Model\Request;
use Keszei\Crud\Action\ListResponse;
use Keszei\Crud\Gateway\FilteringGateway;
use Keszei\Crud\Model\ModelFactory;

class ListAction extends AbstractAction {

	/**
	 * @var ModelFactory
	 */
	private $criteriaFactory;

	/**
	 * @var FilteringGateway
	 */
	private $gateway;

	public function run(Request $request) {
		$this->getResponder()->setResponse(new ListResponse(
			$this->gateway->filter(
				$this->criteriaFactory->create($request)
			)
		));
	}

	public function setGateway(FilteringGateway $gateway) {
		$this->gateway = $gateway;
		return $this;
	}

	public function setCriteriaFactory(ModelFactory $criteriaFactory) {
		$this->criteriaFactory = $criteriaFactory;
		return $this;
	}

}
