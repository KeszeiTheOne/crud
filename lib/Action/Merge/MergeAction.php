<?php

namespace Keszei\Crud\Action\Merge;

use Keszei\Action\Exception\UnexpectedType;
use Keszei\Action\AbstractAction;
use Keszei\Action\Model\Request;
use Keszei\Crud\Action\ModelResponse;
use Keszei\Crud\Gateway\UpdaterGateway;
use Keszei\Crud\Model\ModelFactory;
use Keszei\Crud\Model\ModelUpdater;

class MergeAction extends AbstractAction {

	/**
	 * @var ModelFactory
	 */
	private $criteriaFactory;

	/**
	 * @var UpdaterGateway
	 */
	private $gateway;

	/**
	 * @var ModelFactory
	 */
	private $modelFactory;

	/**
	 * @var ModelUpdater
	 */
	private $modelUpdater;

	public function run(Request $request) {
		$model = $this->findModel($request);

		if (null === $model) {
			$model = $this->createModel($request);
		}
		else {
			$this->modelUpdater->update($model, $request);
		}

		$this->persistAndRespond($model);
	}

	private function persistAndRespond($model) {
		$this->gateway->persist($model);
		$this->getResponder()->setResponse(new ModelResponse($model));
	}

	private function findModel(Request $request) {
		$criteria = $this->criteriaFactory->create($request);

		if (null === $criteria) {
			throw new UnexpectedType;
		}

		return $this->gateway->find($criteria);
	}

	private function createModel(Request $request) {
		$model = $this->modelFactory->create($request);

		if (null === $model) {
			throw new UnexpectedType;
		}

		return $model;
	}

	public function setGateway(UpdaterGateway $gateway) {
		$this->gateway = $gateway;
		return $this;
	}

	public function setCriteriaFactory(ModelFactory $criteriaFactory) {
		$this->criteriaFactory = $criteriaFactory;
		return $this;
	}

	public function setModelFactory(ModelFactory $modelFactory) {
		$this->modelFactory = $modelFactory;
		return $this;
	}

	public function setModelUpdater(ModelUpdater $modelUpdater) {
		$this->modelUpdater = $modelUpdater;
		return $this;
	}

}
