<?php

namespace Keszei\Crud\Action\Exception;

use RuntimeException;

class MissingId extends RuntimeException {
	
}
