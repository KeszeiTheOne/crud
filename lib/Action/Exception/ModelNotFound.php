<?php

namespace Keszei\Crud\Action\Exception;

use RuntimeException;

class ModelNotFound extends RuntimeException {

	private $id;

	public function __construct($id) {
		$this->id = $id;
	}

	public function getId() {
		return $this->id;
	}

}
