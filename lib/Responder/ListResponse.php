<?php

namespace Keszei\Crud\Responder;

use Keszei\Action\Model\Response;

interface ListResponse extends Response {

	public function getModels();
}
