<?php

namespace Keszei\Crud\Responder;

use Keszei\Action\Model\Response;

interface ModelResponse extends Response {

	public function getModel();
}
