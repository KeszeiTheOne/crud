<?php

namespace Keszei\Crud\Gateway;

interface PersisterGateway {

	public function persist($object);
}
