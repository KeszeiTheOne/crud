<?php

namespace Keszei\Crud\Gateway;

interface FilteringGateway {

	public function filter($criteria);
}
