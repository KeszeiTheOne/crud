<?php

namespace Keszei\Crud\Gateway;

interface CrudGateway extends FinderGateway, FilteringGateway, PersisterGateway, UpdaterGateway, RemoverGateway {
	
}
