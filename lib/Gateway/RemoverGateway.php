<?php

namespace Keszei\Crud\Gateway;

interface RemoverGateway extends FinderGateway {

	public function remove($object);
}
