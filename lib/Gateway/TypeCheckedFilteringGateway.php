<?php

namespace Keszei\Crud\Gateway;

use Keszei\Action\Exception\UnexpectedType;

class TypeCheckedFilteringGateway implements FilteringGateway {

	private $className;

	/**
	 * @var FilteringGateway
	 */
	private $gateway;

	public function __construct(FilteringGateway $gateway, $className) {
		$this->gateway = $gateway;
		$this->className = $className;
	}

	public function filter($criteria) {
		$models = $this->gateway->filter($criteria);

		foreach ($models as $model) {
			if (null !== $model && !$model instanceof $this->className) {
				throw new UnexpectedType;
			}
		}

		return $models;
	}

}
