<?php

namespace Keszei\Crud\Gateway;

class CompositeCrudGateway implements CrudGateway {

	/**
	 * @var FilteringGateway
	 */
	private $filter;

	/**
	 * @var FinderGateway
	 */
	private $finder;

	/**
	 * @var PersisterGateway
	 */
	private $persister;

	/**
	 * @var RemoverGateway
	 */
	private $remover;

	public function filter($criteria) {
		return $this->filter->filter($criteria);
	}

	public function find($id) {
		return $this->finder->find($id);
	}

	public function persist($object) {
		return $this->persister->persist($object);
	}

	public function remove($object) {
		return $this->remover->remove($object);
	}

	public function setFilter(FilteringGateway $filter) {
		$this->filter = $filter;
		return $this;
	}

	public function setFinder(FinderGateway $finder) {
		$this->finder = $finder;
		return $this;
	}

	public function setPersister(PersisterGateway $persister) {
		$this->persister = $persister;
		return $this;
	}

	public function setRemover(RemoverGateway $remover) {
		$this->remover = $remover;
		return $this;
	}

}
