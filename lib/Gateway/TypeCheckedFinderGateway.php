<?php

namespace Keszei\Crud\Gateway;

use Keszei\Action\Exception\UnexpectedType;

class TypeCheckedFinderGateway implements FinderGateway {

	private $className;

	/**
	 * @var FinderGateway
	 */
	private $gateway;

	public function __construct(FinderGateway $gateway, $className) {
		$this->gateway = $gateway;
		$this->className = $className;
	}

	public function find($id) {
		$model = $this->gateway->find($id);

		if (null !== $model && !$model instanceof $this->className) {
			throw new UnexpectedType;
		}

		return $model;
	}

}
