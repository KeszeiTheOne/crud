<?php

namespace Keszei\Crud\Gateway;

interface FinderGateway {

	public function find($id);
}
