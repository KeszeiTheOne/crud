<?php

namespace Keszei\Crud\Model;

use Keszei\Action\Exception\UnexpectedType;
use Keszei\Crud\Action\Exception\MissingId;
use Keszei\Crud\Action\IdentifierRequest;

class IdCriteriaFactory implements ModelFactory {

	public function create($request) {
		if (!$request instanceof IdentifierRequest) {
			throw new UnexpectedType();
		}

		if (null === $request->getId()) {
			throw new MissingId();
		}

		return $request->getId();
	}

}
