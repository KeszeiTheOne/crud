<?php

namespace Keszei\Crud\Model;

interface ModelUpdater {

	public function update($model, $data);
}
