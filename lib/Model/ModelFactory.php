<?php

namespace Keszei\Crud\Model;

interface ModelFactory {

	public function create($builder);
}
