<?php

namespace Keszei\Crud\Test\Fixtures\Model\ModelFactory;

class ModelFactoryStub extends ModelFactoryDummy {

	public $model;

	public function __construct($model) {
		$this->model = $model;
	}

	public function create($builder) {
		return $this->model;
	}

}
