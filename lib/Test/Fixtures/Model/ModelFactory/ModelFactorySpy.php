<?php

namespace Keszei\Crud\Test\Fixtures\Model\ModelFactory;

class ModelFactorySpy extends ModelFactoryStub {

	public $builders = [];

	public function create($builder) {
		$this->builders[] = $builder;

		return parent::create($builder);
	}

	public function getCreatedTimes() {
		return count($this->builders);
	}

	public function getLastGivenBuilder() {
		return end($this->$builders);
	}

}
