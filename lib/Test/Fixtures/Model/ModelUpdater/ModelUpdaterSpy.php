<?php

namespace Keszei\Crud\Test\Fixtures\Model\ModelUpdater;

class ModelUpdaterSpy extends ModelUpdaterDummy {

	public $updatedModels;

	public $updaterDatas;

	public function update($model, $data) {
		$this->updatedModels = $model;
		$this->updaterDatas = $data;
	}

	public function getUpdatedModel() {
		return $this->updatedModels;
	}

	public function getUpdaterData() {
		return $this->updaterDatas;
	}

	public function getUpdateTimes() {
		return count($this->updatedModels);
	}

	public function getLastUpdatedModel() {
		return end($this->updatedModels);
	}

	public function getLastUpdaterData() {
		return end($this->updaterDatas);
	}

}
