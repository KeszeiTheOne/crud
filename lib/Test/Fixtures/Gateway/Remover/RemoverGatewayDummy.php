<?php

namespace Keszei\Crud\Test\Fixtures\Gateway\Remover;

use Keszei\Crud\Gateway\RemoverGateway;

class RemoverGatewayDummy implements RemoverGateway {

	public function remove($object) {
		
	}

	public function find($id) {
		
	}

}
