<?php

namespace Keszei\Crud\Test\Fixtures\Gateway\Remover;

use Keszei\Crud\Test\Fixtures\Gateway\FinderGatewayStubTrait;

class RemoverGatewayStub extends RemoverGatewayDummy {

	use FinderGatewayStubTrait;

	public function __construct($model = null) {
		$this->model = $model;
	}

}
