<?php

namespace Keszei\Crud\Test\Fixtures\Gateway\Remover;

use Keszei\Crud\Test\Fixtures\Gateway\FinderGatewaySpyTrait;

class RemoverGatewaySpy extends RemoverGatewayStub {

	use FinderGatewaySpyTrait;

	public $removedObjects = [];

	public function remove($object) {
		$this->removedObjects[] = $object;
	}

	public function getRemovedTimes() {
		return count($this->removedObjects);
	}

	public function getLastRemovedObject() {
		return end($this->removedObjects);
	}

}
