<?php

namespace Keszei\Crud\Test\Fixtures\Gateway;

trait PersisterGatewaySpyTrait {

	public $objects = [];

	public function persist($object) {
		$this->objects[] = $object;
	}

	public function getPersistedTimes() {
		return count($this->objects);
	}

	public function getLastPersistedObject() {
		return end($this->objects);
	}

}
