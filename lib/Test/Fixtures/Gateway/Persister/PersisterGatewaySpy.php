<?php

namespace Keszei\Crud\Test\Fixtures\Gateway\Persister;

use Keszei\Crud\Gateway\PersisterGateway;
use Keszei\Crud\Test\Fixtures\Gateway\PersisterGatewaySpyTrait;

class PersisterGatewaySpy implements PersisterGateway {

	use PersisterGatewaySpyTrait;
}
