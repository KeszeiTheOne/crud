<?php

namespace Keszei\Crud\Test\Fixtures\Gateway\Filtering;

class FilteringGatewayStub extends FilteringGatewayDummy {

	public $models = [];

	public function __construct($models = []) {
		$this->models = $models;
	}

	public function filter($criteria) {
		return $this->models;
	}

}
