<?php

namespace Keszei\Crud\Test\Fixtures\Gateway\Filtering;

class FilteringGatewaySpy extends FilteringGatewayStub {

	public $criterias = [];

	public function filter($criteria) {
		$this->criterias[] = $criteria;

		return parent::filter($criteria);
	}

	public function getFilteringTimes() {
		return count($this->criterias);
	}

	public function getLastFilteredCriteria() {
		return end($this->criterias);
	}

}
