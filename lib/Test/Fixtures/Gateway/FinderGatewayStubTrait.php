<?php

namespace Keszei\Crud\Test\Fixtures\Gateway;

trait FinderGatewayStubTrait {

	public $model;

	public function find($id) {
		return $this->model;
	}

}
