<?php

namespace Keszei\Crud\Test\Fixtures\Gateway\Updater;

use Keszei\Crud\Test\Fixtures\Gateway\FinderGatewayStubTrait;

class UpdaterGatewayStub extends UpdaterGatewayDummy {

	use FinderGatewayStubTrait;

	public function __construct($model = null) {
		$this->model = $model;
	}

}
