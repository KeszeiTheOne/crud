<?php

namespace Keszei\Crud\Test\Fixtures\Gateway\Updater;

use Keszei\Crud\Test\Fixtures\Gateway\FinderGatewaySpyTrait;
use Keszei\Crud\Test\Fixtures\Gateway\PersisterGatewaySpyTrait;

class UpdaterGatewaySpy extends UpdaterGatewayStub {

	use FinderGatewaySpyTrait,
	 PersisterGatewaySpyTrait;
}
