<?php

namespace Keszei\Crud\Test\Fixtures\Gateway\Updater;

use Keszei\Crud\Gateway\UpdaterGateway;

class UpdaterGatewayDummy implements UpdaterGateway {

	public function find($id) {
		
	}

	public function persist($object) {
		
	}

}
