<?php

namespace Keszei\Crud\Test\Fixtures\Gateway;

trait FinderGatewaySpyTrait {

	public $ids;

	public function find($id) {
		$this->ids[] = $id;
		return parent::find($id);
	}

	public function getFindingTimes() {
		return count($this->ids);
	}

	public function getLastFoundedId() {
		return end($this->ids);
	}

}
