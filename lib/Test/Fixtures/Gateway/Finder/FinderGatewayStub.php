<?php

namespace Keszei\Crud\Test\Fixtures\Gateway\Finder;

use Keszei\Crud\Test\Fixtures\Gateway\FinderGatewayStubTrait;

class FinderGatewayStub extends FinderGatewayDummy {

	use FinderGatewayStubTrait;

	public function __construct($model = null) {
		$this->model = $model;
	}

}
