<?php

namespace Keszei\Crud\Test\Fixtures\Gateway\Finder;

use Keszei\Crud\Test\Fixtures\Gateway\FinderGatewaySpyTrait;

class FinderGatewaySpy extends FinderGatewayStub {

	use FinderGatewaySpyTrait;
}
